<?php
$MESS["T_PARAMS_RECIPIENT_EMAIL"] = "E-mail, на который будет отправлено письмо";
$MESS['T_PARAMS_OK_MESSAGE'] = "Сообщение, выводимое пользователю после отправки";
$MESS['T_PARAMS_OK_TEXT'] = "Спасибо, ваша заявка принята.";
$MESS['T_PARAMS_EMAIL_TEMPLATES'] = "Почтовые шаблоны для отправки письма";
$MESS['T_PARAMS_ATTACHE_EXT_ALLOW'] = "Допустимые расширения файлов";
?>