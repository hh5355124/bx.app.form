<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("T_COMP_NAME"),
	"DESCRIPTION" => GetMessage("T_COMP_DESC"),
	"ICON" => "images/news_list.gif",
	"SORT" => 1000,
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "bx.app.form",
		"NAME" => GetMessage("T_COMP_NAME_EXT"),
	)
);
?>