<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__); 

class BxAppForm extends CBitrixComponent
{
	public $arResult = array();

	/**
	 * Create data fields form
	 */
	public function prepareData()
	{
		$this->arResult = array(
			"PARAMS_HASH" => md5(serialize($this->arParams) . $this->GetTemplateName()),
			"FIELDS" => array(
				array(
					"NAME" => Loc::getMessage("T_APP_FORM_TITLE"),
					"CODE" => "TITLE",
					"TYPE" => "TEXT",
					"VALUES" => null
				),
				array(
					"NAME" => Loc::getMessage("T_APP_FORM_SECTION"),
					"CODE" => "CATEGORY",
					"TYPE" => "RADIO",
					"VALUES" => array(
						0 => Loc::getMessage("T_APP_FORM_SEC_1"),
						1 => Loc::getMessage("T_APP_FORM_SEC_2"),
					)
				),
				array(
					"NAME" => Loc::getMessage("T_APP_FORM_TYPE"),
					"CODE" => "TYPE",
					"TYPE" => "RADIO",
					"VALUES" => array(
						0 => Loc::getMessage("T_APP_FORM_TYPE_1"),
						1 => Loc::getMessage("T_APP_FORM_TYPE_2"),
						2 => Loc::getMessage("T_APP_FORM_TYPE_3"),
					)
				),
				array(
					"NAME" => Loc::getMessage("T_APP_FORM_STORE"),
					"CODE" => "STORE",
					"TYPE" => "SELECT",
					"VALUES" => array(
						0 => Loc::getMessage("T_APP_FORM_STORE_1"),
						1 => Loc::getMessage("T_APP_FORM_STORE_2"),
						2 => Loc::getMessage("T_APP_FORM_STORE_3"),
					)
				),
				array(
					"NAME" => Loc::getMessage("T_APP_FORM_COMPOUND"),
					"CODE" => "COMPOUND",
					"TYPE" => "COMPOSITE",
					"VALUES" => array(
						array(
							"NAME" => Loc::getMessage("T_APP_FORM_COMPOUND_1"),
							"CODE" => "COMPOUND_1[]",
							"TYPE" => "SELECT",
							"VALUES" => array(
								0 => Loc::getMessage("T_APP_FORM_COMPOUND_1_V_0"),
								1 => Loc::getMessage("T_APP_FORM_COMPOUND_1_V_1"),
								2 => Loc::getMessage("T_APP_FORM_COMPOUND_1_V_2"),
							)
						),
						array(
							"NAME" => Loc::getMessage("T_APP_FORM_COMPOUND_2"),
							"CODE" => "COMPOUND_2[]",
							"TYPE" => "TEXT",
							"VALUES" => null
						),
						array(
							"NAME" => Loc::getMessage("T_APP_FORM_COMPOUND_3"),
							"CODE" => "COMPOUND_3[]",
							"TYPE" => "TEXT",
							"VALUES" => null
						),
						array(
							"NAME" => Loc::getMessage("T_APP_FORM_COMPOUND_4"),
							"CODE" => "COMPOUND_4[]",
							"TYPE" => "TEXT",
							"VALUES" => null
						),
						array(
							"NAME" => Loc::getMessage("T_APP_FORM_COMPOUND_5"),
							"CODE" => "COMPOUND_5[]",
							"TYPE" => "TEXT",
							"VALUES" => null
						),
					)
				),
				array(
					"NAME" => Loc::getMessage("T_APP_FORM_FILE"),
					"CODE" => "FILE",
					"TYPE" => "FILE",
					"VALUES" => null
				),
				array(
					"NAME" => Loc::getMessage("T_APP_FORM_COMMENT"),
					"CODE" => "COMMENT",
					"TYPE" => "TEXTAREA",
					"VALUES" => null
				),
			)
		);

		/** set cache keys */
		$this->SetResultCacheKeys(array("FIELDS"));
	}

	/** Prepare text email message */
	private function prepareText($data)
	{
		$result = "";

		if(!is_array($data))
			return false;

		if(!is_countable($data))
			return false;

		if(is_array($this->arResult["FIELDS"]))
		{
			if(is_countable($this->arResult["FIELDS"]))
			{
				foreach($this->arResult["FIELDS"] as $field)
				{
					switch ($field['TYPE'])
					{
						case "TEXTAREA":
							if($data[$field['CODE']])
							{
								$result .= $field['NAME'] . ': ' . $data[$field['CODE']] . PHP_EOL;
							}
							break;
						case "RADIO":
							if($data[$field['CODE']])
							{
								$result .= $field['NAME'] . ': ' . $field['VALUES'][$data[$field['CODE']]] . PHP_EOL;
							}
							break;
						case "SELECT":
							if($data[$field['CODE']])
							{
								$result .= $field['NAME'] . ': ' . $field['VALUES'][$data[$field['CODE']]] . PHP_EOL;
							}
							break;
						case "COMPOSITE":
							foreach($field['VALUES'] as $fild)
							{
								if($data[str_replace('[]', '', $fild['CODE'])])
								{
									$result .= $fild['NAME'] . ': ' . PHP_EOL;

									if($fild['TYPE']=='SELECT')
									{
										foreach ($data[str_replace('[]', '', $fild['CODE'])] as $k => $v)
										{
											$result .= $fild['VALUES'][$v] . PHP_EOL;
										}
									}else{
										foreach ($data[str_replace('[]', '', $fild['CODE'])] as $k => $v)
										{
											$result .= $v . PHP_EOL;
										}
									}

									$result .= '----------------' . PHP_EOL;
								}
							}
							break;
						default: //TEXT
							if($data[$field['CODE']])
							{
								$result .= $field['NAME'] . ': ' . $data[$field['CODE']] . PHP_EOL;
							}
							break;
					}
				}
			}
		}

		return $result;
	}

	/** Send message to email */
	private function send()
	{
		if($_SERVER["REQUEST_METHOD"] == "POST" && (!isset($_POST["PARAMS_HASH"]) || $this->arResult["PARAMS_HASH"] === $_POST["PARAMS_HASH"]))
		{
			$this->arResult["ERROR_MESSAGE"] = array();
			$this->arParams["EVENT_NAME"] = "FEEDBACK_FORM";
			if(check_bitrix_sessid())
			{
				/** check required fields */
				if(mb_strlen($_POST["TITLE"]) <= 1)
					$this->arResult["ERROR_MESSAGE"][] = Loc::getMessage("T_APP_FORM_RQ_TITLE");
				if(!isset($_POST["CATEGORY"]))
					$this->arResult["ERROR_MESSAGE"][] = Loc::getMessage("T_APP_FORM_RQ_CATEGORY");
				if(!isset($_POST["TYPE"]))
					$this->arResult["ERROR_MESSAGE"][] = Loc::getMessage("T_APP_FORM_RQ_TYPE");

				/** Check file ext allowed */
				if(isset($_FILES['FILE']))
				{
					if(empty($this->arParams["ATTACHE_EXT_ALLOW"]))
						$this->arParams["ATTACHE_EXT_ALLOW"] = "pdf,docx,doc,txt,xlsx,xls,csv,jpeg,png,jpg";

					$arExtAllowed = explode(",", $this->arParams["ATTACHE_EXT_ALLOW"]);
					
					if(!empty($_FILES['FILE']['full_path']))
					{
						if($extFile = GetFileExtension($_FILES['FILE']['full_path']))
						{
							if(!in_array($extFile, $arExtAllowed))
							{
								$this->arResult["ERROR_MESSAGE"][] = Loc::getMessage("T_APP_FORM_ATTACHE_EXT_ALLOW_ERR", array("#ATTACHE_EXT_ALLOW#" => $this->arParams["ATTACHE_EXT_ALLOW"]));
							}
						}else{
							$this->arResult["ERROR_MESSAGE"][] = Loc::getMessage("T_APP_FORM_ATTACHE_UPLOAD_EXT_ERR");
						}
					}else{
						$this->arResult["ERROR_MESSAGE"][] = Loc::getMessage("T_APP_FORM_ATTACHE_UPLOAD_ERR");
					}
				}
				
				if(empty($this->arResult["ERROR_MESSAGE"]))
				{
					$arFields = Array(
						"AUTHOR" => $_POST["TITLE"],
						"AUTHOR_EMAIL" => $this->arParams["EMAIL_TO"],
						"EMAIL_TO" => $this->arParams["EMAIL_TO"],
						"TEXT" => $this->prepareText($_POST),
					);

					/*** Save attache file to bitrix ***/
					$arFileIds = array();
					if(isset($_FILES['FILE']))
					{
						$arFile = Array("name" => $_FILES['FILE']['name'],
							"size" => $_FILES['FILE']['size'],
							"tmp_name" => $_FILES['FILE']['tmp_name'],
							"type" => "",
							"old_file" => "",
							"del" => "Y",
							"MODULE_ID" => "main",
						);
						$arFileIds[] = \CFile::SaveFile($arFile, "main");
					}

					if(!empty($this->arParams["EVENT_MESSAGE_ID"]))
					{
						foreach($this->arParams["EVENT_MESSAGE_ID"] as $v)
							if(intval($v) > 0)
								\CEvent::Send($this->arParams["EVENT_NAME"], SITE_ID, $arFields, "N", intval($v), $arFileIds);
					}else{
						\CEvent::Send($this->arParams["EVENT_NAME"], SITE_ID, $arFields, "N", "", $arFileIds);
					}
					$event = new \Bitrix\Main\Event('main', 'onBxAppFormFormSubmit', $arFields);
					$event->send();
					global $APPLICATION;
					LocalRedirect($APPLICATION->GetCurPageParam("success=" . $this->arResult["PARAMS_HASH"], Array("success")));
				}
			}else{
				$this->arResult["ERROR_MESSAGE"][] = GetMessage("T_APP_FORM_SESS_EXP");
			}

		}elseif($_REQUEST["success"] == $this->arResult["PARAMS_HASH"])
		{
			$this->arResult["OK_MESSAGE"] = $this->arParams["OK_TEXT"];
		}
	}

	/** Execution component */
	public function executeComponent()
	{
		global $APPLICATION;
		/** set page title */
		$APPLICATION->SetTitle(Loc::getMessage('T_APP_FORM_TITLE_FORM'));
		$this->arParams['COMPONENT_TEMPLATE'] = (empty($this->arParams['COMPONENT_TEMPLATE']))? ".default" : $this->arParams['COMPONENT_TEMPLATE'];

		/** add assets (include CSS & JS) */
		\Bitrix\Main\UI\Extension::load("ui.bootstrap4");
		$APPLICATION->SetAdditionalCSS($this->GetPath() . '/templates/' . $this->arParams['COMPONENT_TEMPLATE'] . '/style.css');

		CJSCore::RegisterExt('bx_app_form_js', array(
			'js' => array(
				$this->GetPath() . '/templates/' . $this->arParams['COMPONENT_TEMPLATE'] . '/script.js',
				'https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.js'
			),
			'lang' => $this->GetPath() . '/templates/' . $this->arParams['COMPONENT_TEMPLATE'] . '/lang/' . LANGUAGE_ID . '/script_js.php',
			'rel' => array('window', 'jquery')
		));
		CJSCore::Init(array('jquery', 'bx_app_form_js'));

		if ($this->StartResultCache(false, array()))
		{
			//prepare data fields
			$this->prepareData();

			//send message
			$this->send();

			//include template
			$this->includeComponentTemplate();

		}else{
			$this->abortResultCache();
		}
		
	}
}
?>