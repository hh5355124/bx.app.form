<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$site = ($_REQUEST["site"] <> ''? $_REQUEST["site"] : ($_REQUEST["src_site"] <> ''? $_REQUEST["src_site"] : false));
$arFilter = Array("TYPE_ID" => "FEEDBACK_FORM", "ACTIVE" => "Y");
if($site !== false)
	$arFilter["LID"] = $site;

$arEvent = Array();
$dbType = CEventMessage::GetList("id", "desc", $arFilter);
while($arType = $dbType->GetNext())
	$arEvent[$arType["ID"]] = "[".$arType["ID"]."] ".$arType["SUBJECT"];

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"OK_TEXT" => Array(
			"NAME" => GetMessage("T_PARAMS_OK_MESSAGE"), 
			"TYPE" => "STRING",
			"DEFAULT" => GetMessage("T_PARAMS_OK_TEXT"), 
			"PARENT" => "BASE",
		),
		"EMAIL_TO" => Array(
			"NAME" => GetMessage("T_PARAMS_RECIPIENT_EMAIL"), 
			"TYPE" => "STRING",
			"DEFAULT" => htmlspecialcharsbx(COption::GetOptionString("main", "email_from")), 
			"PARENT" => "BASE",
		),
		"ATTACHE_EXT_ALLOW" => Array(
			"NAME" => GetMessage("T_PARAMS_ATTACHE_EXT_ALLOW"), 
			"TYPE" => "STRING",
			"DEFAULT" => 'pdf,docx,doc,txt,xlsx,xls,csv,jpeg,png,jpg',
			"PARENT" => "BASE",
		),
		"EVENT_MESSAGE_ID" => Array(
			"NAME" => GetMessage("T_PARAMS_EMAIL_TEMPLATES"), 
			"TYPE"=>"LIST", 
			"VALUES" => $arEvent,
			"DEFAULT"=>"", 
			"MULTIPLE"=>"Y", 
			"COLS"=>25, 
			"PARENT" => "BASE",
		),
		"CACHE_TIME"  =>  Array("DEFAULT"=>36000000),
	)
);
?>