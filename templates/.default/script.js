/** Add line field composite */
function compositeBtnPls(el)
{
    var count = $('.row-composite').length;
    var i = count-1;
    var rowComposite = $('.row-composite')[i];
    var rowCompositeClone = $($('.row-composite')[0]).clone();

    //clear val from clone fields
    $(rowCompositeClone).find('input[type="text"]').each(function(){
        $(this).val("");
    });

    $(rowComposite).after($(rowCompositeClone));
}


/** Remove line field composite */
function compositeBtnMin(el)
{
    var parent = $(el).parents('.row-composite');
    var count = $('.row-composite').length;
    if(count>1){
        $(parent).remove();
    }
}


$(document).ready(function(){

    /** Form validation */
    $('#bx_app_form_js').validate(
    {
        rules:
        {
            TITLE:{ required:true },
            CATEGORY:{ required:true },
            TYPE:{ required:true },
        },
        messages:
        {
            TITLE:
            {
                required: BX.message("T_APP_FORM_REQUIRED_ALERT_ERROR")
            },
            CATEGORY:
            {
                required: BX.message("T_APP_FORM_REQUIRED_ALERT_ERROR")
            },
            TYPE:
            {
                required: BX.message("T_APP_FORM_REQUIRED_ALERT_ERROR")
            }
        },
        errorPlacement: function(error, element) 
        {

        error.appendTo( element.parents('.required').find('strong') );

        }
    });

});