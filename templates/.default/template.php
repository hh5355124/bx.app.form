<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>
<?
use Bitrix\Main;
use Bitrix\Main\Localization\Loc;
?>
<div class="bx_app_form">
    <form id="bx_app_form_js" action="<?=POST_FORM_ACTION_URI?>" method="POST" enctype="multipart/form-data">

        <div class="container-fluid">

            <!-- MESSAGE -->
            <div class="row">
                <div class="col-12">
                <?if(!empty($arResult["ERROR_MESSAGE"])): ?>
                    <div class="alert alert-danger" role="alert">
                    <? foreach($arResult["ERROR_MESSAGE"] as $v): ?>
                        <?=$v;?><br/>
                    <? endforeach; ?>
                    </div>
                <? else: ?>    
                    <? if($arResult["OK_MESSAGE"] <> ''): ?>
                        <div class="alert alert-success" role="alert">
                            <?=$arResult["OK_MESSAGE"]?>
                        </div>
                    <? endif; ?>
                <? endif; ?>
                </div>
            </div>
            <!-- X MESSAGE -->

            <!-- FIELDS -->
            <? if(is_countable($arResult["FIELDS"])): ?>
                <? foreach($arResult["FIELDS"] as $field): ?>

                    <? 
                    switch($field['TYPE'])
                    {
                        case 'RADIO': ?>
                            <div class="row">
                                <div class="col-12 required">
                                    <strong><?=$field['NAME'];?></strong>
                                    <? if(is_countable($field['VALUES'])):?>
                                        <? foreach($field['VALUES'] as $key=>$value): ?>
                                            <p>
                                                <label for="<?=$field['CODE'];?>_<?=$key;?>">
                                                    <input id="<?=$field['CODE'];?>_<?=$key;?>" type="radio" name="<?=$field['CODE'];?>" value="<?=$key;?>"/>
                                                    <?=$value;?>
                                                </label>
                                            </p>
                                        <? endforeach; ?>    
                                    <? endif; ?>
                                </div>
                            </div>
                            <? break;

                        case 'SELECT': ?>
                            <div class="row">
                                <div class="col-12">
                                    <strong><?=$field['NAME'];?></strong>
                                    <p>
                                        <select name="<?=$field['CODE'];?>">
                                            <option value="">---</option>
                                        <? if(is_countable($field['VALUES'])):?>
                                            <? foreach($field['VALUES'] as $key=>$value): ?>
                                            <option value="<?=$key;?>"><?=$value; ?></value>
                                            <? endforeach; ?>    
                                        <? endif; ?>
                                        </select>
                                    </p>
                                </div>
                            </div> 
                            <? break;

                        case 'COMPOSITE': ?>

                            <div class="row">
                                <div class="col-12">
                                    <strong><?=$field['NAME'];?></strong>
                                </div>
                            </div>
                            
                            <div class="row row-composite">
                                <? if(is_countable($field['VALUES'])):?>
                                    <? foreach($field['VALUES'] as $fild): ?>
                                    <div class="col-sm-12 col-md-6 col-lg-4 col-xl-2">
                                        <strong><?=$fild['NAME'];?></strong>
                                        <? 
                                        switch ($fild['TYPE'])
                                        {
                                            case 'SELECT': ?>
                                                <? if(is_countable($fild['VALUES'])):?>
                                                    <p><select name="<?=$fild['CODE'];?>">
                                                    <? foreach($fild['VALUES'] as $key=>$val): ?>
                                                        <option value="<?=$key;?>"><?=$val;?></option>
                                                    <? endforeach; ?>
                                                    </select></p>
                                                <? endif; ?>
                                            <? break;
                                            default:?>
                                                <p><input type="text" name="<?=$fild['CODE'];?>" value="<?=$fild['VALUE'];?>"/></p>
                                            <? break;
                                        }
                                        ?>
                                    </div>
                                    <? endforeach; ?>    
                                <? endif; ?>

                                <div class="col-sm-12 col-md-6 col-lg-4 col-xl-2">
                                    <div class="container-fluid composite-btn">
                                        <div class="row">
                                            <div class="col-6">
                                                <button onclick="compositeBtnPls(this);" type="button" class="btn btn-primary btn-pls" data-toggle="button" aria-pressed="false" autocomplete="off">
                                                    +
                                                </button>
                                            </div>
                                            <div class="col-6">
                                                <button onclick="compositeBtnMin(this);" type="button" class="btn btn-primary btn-min" data-toggle="button" aria-pressed="false" autocomplete="off">
                                                    -
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            
                            <? break;

                        case 'FILE': ?>
                            <div class="row">
                                <div class="col-12">
                                    <strong><?=$field['NAME'];?></strong>
                                    <p>
                                        <input type="FILE" name="<?=$field['CODE'];?>" />
                                    </p>
                                </div>
                            </div> 
                            <? break;

                        case 'TEXTAREA': ?>
                            <div class="row">
                                <div class="col-12">
                                    <strong><?=$field['NAME'];?></strong>
                                    <p>
                                        <textarea name="<?=$field['CODE'];?>" cols="50" rows="5"></textarea>
                                    </p>
                                </div>
                            </div> 
                            <? break;

                        default: ?>
                            <div class="row">
                                <div class="col-12 <?if($field['CODE']=='TITLE'):?>required<?endif;?>">
                                    <strong><?=$field['NAME'];?></strong>
                                    <p><input type="text" name="<?=$field['CODE'];?>" value="<?=$field['VALUE'];?>"/></p>
                                </div>
                            </div>
                            <? break;
                    }
                    ?>

                <? endforeach; ?>

                <div class="row">
                    <div class="col-12">
                        <button type="submit" class="btn btn-primary" data-toggle="submit" aria-pressed="false" autocomplete="off">
                            <?=Loc::getMessage('T_APP_FORM_SUBMIT'); ?>
                        </button>
                    </div>
                </div>

            <? else: ?>
                <div class="alert alert-danger" role="alert"><?=Loc::getMessage('T_APP_EMPTY_FIELDS'); ?></div>
            <? endif; ?>        
        </div>

        <input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
        <?=bitrix_sessid_post('sessid'); ?>
        <!-- X FIELDS -->
    </form>
</div>

<?
    //debug($arResult);
    //debug($_POST);
?>