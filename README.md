Компонент, формирует сообщение с данными полей формы и отправляет по Email шаблону, стандартного почтового события FEEDBACK_FORM.
Добавляется на страницу стандартно через визуальный редактор, из секции "Формы".

Пример вызова компонента для страницы:

```
<?$APPLICATION->IncludeComponent(
	"bx.app.form",
	"",
	Array(
		"ATTACHE_EXT_ALLOW" => "pdf,docx,doc,txt,xlsx,xls,csv,jpeg,png,jpg",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"EMAIL_TO" => "simple@yandex.ru",
		"EVENT_MESSAGE_ID" => array(),
		"OK_TEXT" => "Спасибо, ваша заявка принята."
	)
);?>
```
